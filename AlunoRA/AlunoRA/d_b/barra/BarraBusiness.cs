﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.barra
{
    class BarraBusiness
    {
        BarraDatabase db = new BarraDatabase();

        public int Salvar (BarraDTO dto)
        {
            int id = db.Salvar(dto);
            return id;
        }
    }
}
