﻿using AlunoRA.d_b.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.barra
{
    class BarraDatabase
    {
        Database db = new Database();

        public int Salvar (BarraDTO dto)
        {
            string script = @"INSERT INTO tb_barra(img_barra, id_aluno)
                              VALUES (@img_barra, @id_aluno)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("img_barra", dto.image));
            parms.Add(new MySqlParameter("id_aluno", dto.IdAluno));


            int id = db.ExecuteInsertScriptWithPk(script, parms);
            return id;
            
        }

    }
}
