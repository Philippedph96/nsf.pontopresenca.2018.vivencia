﻿using AlunoRA.d_b.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.Aluno
{
    class AlunoDatabase
    {
        Database db = new Database();
        public int Salvar(AlunoDTO dto)
        {
            string script = @"INSERT INTO tb_aluno (nm_nome, dt_nascimento,ds_email, ds_celular,ds_telefone,ds_ra,ds_cep,ds_numero,ds_complemento,dt_ano) 
                                   VALUES (@nm_nome, @dt_nascimento,@ds_email, @ds_celular,@ds_telefone,@ds_ra,@ds_cep,@ds_numero,@ds_complemento,@dt_ano)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("ds_email", dto.email));
            parms.Add(new MySqlParameter("ds_celular", dto.celular));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_ra", dto.ra));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_complemento", dto.complemento));
            parms.Add(new MySqlParameter("ds_numero", dto.numero));
            parms.Add(new MySqlParameter("dt_ano", dto.ano));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<AlunoDTO> Consultar(AlunoDTO dto)
        {
            string script = @"SELECT * FROM `PontoDB`.`tb_aluno` WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome","%" + dto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AlunoDTO> lista = new List<AlunoDTO>();
            while (reader.Read())
            {
                AlunoDTO dtos = new AlunoDTO();
                dtos.ID = reader.GetInt32("id_aluno");
                dtos.nome = reader.GetString("nm_nome");
                dtos.nascimento = reader.GetDateTime("dt_nascimento");
                dtos.telefone = reader.GetString("ds_telefone");
                dtos.celular = reader.GetString("ds_celular");
                dtos.email = reader.GetString("ds_email");
                dtos.cep = reader.GetString("ds_cep");
                dtos.complemento = reader.GetString("ds_complemento");
                dtos.numero = reader.GetInt32("ds_numero");
                dtos.ano = reader.GetInt32("dt_ano");
                dto.ra = reader.GetString("ds_ra");
                

                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }

        public List<AlunoDTO> ConsultarporRa(AlunoDTO dto)
        {
            string script = @"SELECT * FROM `PontoDB`.`tb_aluno` WHERE ds_ra = @ds_ra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_ra",  dto.ra));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AlunoDTO> lista = new List<AlunoDTO>();
            while (reader.Read())
            {
                AlunoDTO dtos = new AlunoDTO();
                dtos.ID = reader.GetInt32("id_aluno");
                dtos.nome = reader.GetString("nm_nome");
                dtos.nascimento = reader.GetDateTime("dt_nascimento");
                dtos.telefone = reader.GetString("ds_telefone");
                dtos.celular = reader.GetString("ds_celular");
                dtos.email = reader.GetString("ds_email");
                dtos.cep = reader.GetString("ds_cep");
                dtos.complemento = reader.GetString("ds_complemento");
                dtos.numero = reader.GetInt32("ds_numero");
                dtos.ano = reader.GetInt32("dt_ano");
                dto.ra = reader.GetString("ds_ra");


                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
        public List<AlunoDTO> Consultar2(string nome)
        {
            string script = @" SELECT * FROM tb_aluno
                                WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AlunoDTO> listaDTO = new List<AlunoDTO>();

            while (reader.Read())
            {
                AlunoDTO dto = new AlunoDTO();
                dto.ID = reader.GetInt32("id_aluno");
                dto.nome = reader.GetString("nm_nome");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.email = reader.GetString("ds_email");
                dto.celular = reader.GetString("ds_celular");
                dto.telefone = reader.GetString("ds_telefone");
                dto.ra = reader.GetString("ds_ra");
                dto.cep = reader.GetString("ds_cep");
                dto.numero = reader.GetInt32("ds_numero");
                dto.complemento = reader.GetString("ds_complemento");
                dto.ano = reader.GetInt32("dt_ano");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;
        }
        public List<AlunoDTO> ConsultarPorId(int id)
        {
            string script = @" SELECT * FROM tb_aluno
                                WHERE id_aluno = @id_aluno";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", id ));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AlunoDTO> listaDTO = new List<AlunoDTO>();

            while (reader.Read())
            {
                AlunoDTO dto = new AlunoDTO();
                dto.ID = reader.GetInt32("id_aluno");
                dto.nome = reader.GetString("nm_nome");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.email = reader.GetString("ds_email");
                dto.celular = reader.GetString("ds_celular");
                dto.telefone = reader.GetString("ds_telefone");
                dto.ra = reader.GetString("ds_ra");
                dto.cep = reader.GetString("ds_cep");
                dto.numero = reader.GetInt32("ds_numero");
                dto.complemento = reader.GetString("ds_complemento");
                dto.ano = reader.GetInt32("dt_ano");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;
        }

        public List<AlunoDTO> Listar()
        {
            string script = @"SELECT * FROM `PontoDB`.`tb_aluno`";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AlunoDTO> lista = new List<AlunoDTO>();
            while (reader.Read())
            {
                AlunoDTO dto = new AlunoDTO();
                dto.ID = reader.GetInt32("id_aluno");
                dto.nome = reader.GetString("nm_nome");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.telefone = reader.GetString("ds_telefone");
                dto.celular = reader.GetString("ds_celular");
                dto.email = reader.GetString("ds_email");
                dto.cep = reader.GetString("ds_cep");
                dto.complemento = reader.GetString("ds_complemento");
                dto.numero = reader.GetInt32("ds_numero");
                dto.ano = reader.GetInt32("dt_ano");
                dto.ra = reader.GetString("ds_ra");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Alterar (AlunoDTO dto)
        {
            string script = @" UPDATE tb_aluno
                                 SET nm_nome = @nm_nome,
                                     dt_nascimento = @dt_nascimento,
                                     ds_email = @ds_email,
                                     ds_celular = @ds_celular,
                                     ds_telefone = @ds_telefone,
                                     ds_ra = @ds_ra,
                                     ds_cep = @ds_cep,
                                     ds_numero = @ds_numero,
                                     ds_complemento = @ds_complemento,
                                     dt_ano = @dt_ano";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_aluno", dto.ID));
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("ds_email", dto.email));
            parms.Add(new MySqlParameter("ds_celular", dto.celular));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_ra", dto.ra));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_numero", dto.numero));
            parms.Add(new MySqlParameter("ds_complemento", dto.complemento));
            parms.Add(new MySqlParameter("dt_ano", dto.ano));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(AlunoDTO dto)
        {
            string script = @"DELETE  FROM tb_aluno WHERE id_aluno = @id_aluno";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_aluno", dto.ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
