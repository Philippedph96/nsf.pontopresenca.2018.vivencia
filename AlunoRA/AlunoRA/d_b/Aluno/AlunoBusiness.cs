﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlunoRA.d_b.Aluno
{
    class AlunoBusiness
    {
        AlunoDatabase db = new AlunoDatabase();
        public int Salvar(AlunoDTO dto)
        {          

            if (dto.nome == string.Empty)
            {
                MessageBox.Show("Informe o nome do Aluno");
            }

            if (dto.numero == 0)
            {
                MessageBox.Show("Infome o numero do Aluno");
            }

            if (dto.telefone == string.Empty)
            {
                MessageBox.Show("Informe telefone o do Aluno");
            }

            if (dto.cep == string.Empty)
            {
                MessageBox.Show("Informe o cep do Aluno");
            }


            DateTime MenorDeIdade = Convert.ToDateTime("2002/01/01");
            bool dataInvalida = (dto.nascimento >= MenorDeIdade);

            if (dataInvalida == true)
            {
                MessageBox.Show("A idade do Aluno é invalida");
            }

            return db.Salvar(dto);
        }
        public List<AlunoDTO> Consultar(AlunoDTO dto)
        {
            
            return db.Consultar(dto);
        }
        public List<AlunoDTO> Listar()
        {
          
            return db.Listar();
        }


        public void Alterar(AlunoDTO dto)
            {
            

            db.Alterar(dto);
            }
        public void Remover(AlunoDTO dto)
        {
            db.Remover(dto);
        }


         public List<AlunoDTO> Consultar2 (string nome)
        {
            
            if ( nome == "")
            {
                nome = string.Empty;
            }

            return db.Consultar2(nome);
        }
        public List<AlunoDTO> ConsultarPorId(int id)
        {
            return db.ConsultarPorId(id);
        }



    }
}
