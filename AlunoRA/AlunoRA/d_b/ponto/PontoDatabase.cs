﻿//using AlunoRA.bazze;
using AlunoRA.d_b.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.ponto
{
    public class PontoDatabase
    {
        public int Salvar(PontoDTO dto)
        {
            string script = @"INSERT INTO tb_ponto(

                            dt_entrada,
                            dt_saida,
                            tb_aluno_id_aluno,
                            ds_observacao)

            VALUES(                                    
                           @dt_entrada,
                           @dt_saida,
                           @tb_aluno_id_aluno,
                          @ds_observacao)
                            ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_saida", dto.Saida));
            parms.Add(new MySqlParameter("dt_entrada", dto.Entrada));
            parms.Add(new MySqlParameter("tb_aluno_id_aluno", dto.Id_Aluno));
            parms.Add(new MySqlParameter("ds_observacao", dto.Observacoes));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);



        }

        public void Alterar(PontoDTO dto)
        {
            string script = @"UPDATE tb_ponto SET 
                            dt_saida            =@dt_saida

                WHERE tb_aluno_id_aluno   =@tb_aluno_id_aluno";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_saida", dto.Saida));
            parms.Add(new MySqlParameter("dt_entrada", dto.Entrada));
            parms.Add(new MySqlParameter("tb_aluno_id_aluno", dto.Id_Aluno));
            parms.Add(new MySqlParameter("ds_observacao", dto.Observacoes));

            Database db = new Database();
             db.ExecuteInsertScript(script, parms);
        }

        public void Remover(PontoDTO dto)
        {
            string script = @"DELETE FROM tb_ponto WHERE id_ponto = @id_ponto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_ponto", dto.ID));
            
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PontoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_ponto";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<PontoDTO> lista = new List<PontoDTO>();
            while(reader.Read())
            {
                PontoDTO dto = new PontoDTO();
                dto.ID = reader.GetInt32("id_ponto");
                dto.Saida = reader.GetDateTime("hr_saida");
                dto.Entrada = reader.GetDateTime("hr_entrada");
                dto.Ponto = reader.GetDateTime("dt_ponto");
                dto.Observacoes = reader.GetString("ds_observacao");
                dto.Id_Aluno = reader.GetInt32("tb_aluno_id_aluno");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<PontoDTO> ConsultarPorAluno(PontoDTO dt)
        {
            string script = @"SELECT * FROM tb_ponto WHERE tb_aluno_id_aluno =@tb_aluno_id_aluno";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("tb_aluno_id_aluno", dt.Id_Aluno));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PontoDTO> lista = new List<PontoDTO>();
            while (reader.Read())
            {
                PontoDTO dto = new PontoDTO();
                dto.ID = reader.GetInt32("id_ponto");
                dto.Saida = reader.GetDateTime("hr_saida");
                dto.Entrada = reader.GetDateTime("hr_entrada");
                dto.Ponto = reader.GetDateTime("dt_ponto");
                dto.Observacoes = reader.GetString("ds_observacao");
                dto.Id_Aluno = reader.GetInt32("tb_aluno_id_aluno");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public List<View_ponto> ConsultarView (View_ponto dt)
        {
            string script = @"SELECT * FROM view_ponto WHERE nm_nome = @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dt.Aluno));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<View_ponto> lista = new List<View_ponto>();

            while (reader.Read())
            {
                View_ponto dto = new View_ponto();
                dto.ID = reader.GetInt32("id_ponto");
                dto.Saida = reader.GetDateTime("dt_saida");
                dto.Entrada = reader.GetDateTime("dt_entrada");
                dto.Observacoes = reader.GetString("ds_observacao");
                dto.Aluno = reader.GetString("nm_nome");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
