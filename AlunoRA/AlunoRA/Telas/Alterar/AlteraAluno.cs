﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.Aluno;

namespace AlunoRA.Telas.Alterar
{
    public partial class AlteraAluno : UserControl
    {
        public AlteraAluno()
        {
            InitializeComponent();
        }

        AlunoDTO dto;

        public void Loadscreen(AlunoDTO dto)
        {
            this.dto = dto;
            txtnome.Text = dto.nome;
            txtano.Text = dto.ano.ToString();
            txtnascimento.Text = dto.nascimento.ToString();
            txtemail.Text = dto.email;
            txtra.Text = dto.ra;
            txttelefone.Text = dto.telefone;
            txtcelular.Text = dto.celular;
            txtcep.Text = dto.cep;
            txtcomplemento.Text = dto.complemento;
            txtnumeor.Text = dto.numero.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AlunoBusiness business = new AlunoBusiness();
            dto.nome = txtnome.Text;
            dto.ano = Convert.ToInt32(txtano.Text);
            dto.nascimento = Convert.ToDateTime(txtnascimento.Text);
            dto.email = txtemail.Text;
            dto.ra = txtra.Text;
            dto.telefone = txttelefone.Text;
            dto.celular = txtcelular.Text;
            dto.cep = txtcep.Text;
            dto.complemento = txtcomplemento.Text;
            dto.numero = Convert.ToInt32(txtnumeor.Text);

            business.Alterar(dto);

            MessageBox.Show("Alterado com sucesso!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            Incial frm = new Incial();
            frm.Show();
            Hide();
        }

        private void AlteraAluno_Load(object sender, EventArgs e)
        {

        }
    }
}
