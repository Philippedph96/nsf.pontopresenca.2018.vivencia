﻿using AlunoRA.Telas.Alterar;
using AlunoRA.Telas.Aluno;
using AlunoRA.Telas.Cadastrar;
using AlunoRA.Telas.Consultar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlunoRA.Telas
{
    public partial class Incial : Form
    {
        public static Incial Atual;
        public Incial()
        {
            InitializeComponent();
        }
        public void OpenScreen (UserControl control)
        {
            if (pnlconteudo.Controls.Count == 1)
                pnlconteudo.Controls.RemoveAt(0);
            pnlconteudo.Controls.Add(control);
                    
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void pnlConteudo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //referencia da tela cadastro de aluno nao encontrada
            CadastrarAluno tela = new CadastrarAluno();
            OpenScreen(tela);
        }

        private void pnlconteudo_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Aluno.UserControl2 tela = new Aluno.UserControl2();
            OpenScreen(tela);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            UserControl1 tela = new UserControl1();
            OpenScreen(tela);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Consultar.UserControl2 tela = new Consultar.UserControl2();
            OpenScreen(tela);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Consultar.UserControl3 tela = new Consultar.UserControl3();
            OpenScreen(tela);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BaterEntrada tela = new BaterEntrada();
            OpenScreen(tela);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            BaterSaida tela = new BaterSaida();
            OpenScreen(tela);
        }
    }
}
