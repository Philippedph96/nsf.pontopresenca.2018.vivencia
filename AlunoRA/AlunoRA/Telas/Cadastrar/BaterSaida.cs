﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.ponto;
using AlunoRA.d_b.Aluno;

namespace AlunoRA.Telas.Cadastrar
{
    public partial class BaterSaida : UserControl
    {
        public BaterSaida()
        {
            InitializeComponent();
        }

        private void BaterSaida_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PontoDTO dto = new PontoDTO();
            dto.Id_Aluno = Convert.ToInt32(lblid.Text);
            dto.Saida = DateTime.Now;

            PontoBusiness bs = new PontoBusiness();
            bs.Alterar(dto);

            MessageBox.Show("Ponto efetuado com sucesso.", "Vivência Bruno",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Imagens (*.png) |* .png";

            DialogResult resultado = dialog.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                BarCode barcode = new BarCode();
                string dado = barcode.LerCodigoBarras(dialog.FileName);

                lblcodigo.Text = dado;

            }

            AlunoDTO dto = new AlunoDTO();
            dto.ra = lblcodigo.Text.Trim();

            AlunoDatabase business = new AlunoDatabase();
            List<AlunoDTO> consultar = business.ConsultarporRa(dto);
            foreach (AlunoDTO item in consultar)
            {
                lblnome.Text = item.nome;
                lblemail.Text = item.email;
                lblano.Text = item.ano.ToString();
                lblid.Text = item.ID.ToString();
            }

        }
    }
}
