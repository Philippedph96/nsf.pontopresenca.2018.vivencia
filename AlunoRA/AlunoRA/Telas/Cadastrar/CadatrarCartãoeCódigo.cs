﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.Aluno;
using AlunoRA.d_b.barra;
using AlunoRA.Imagem;

namespace AlunoRA.Telas.Aluno
{
    public partial class UserControl2 : UserControl
    {
        public UserControl2()
        {
            InitializeComponent();
            CarregarCombo();
        }

        public void CarregarCombo()
        {
            AlunoBusiness business = new AlunoBusiness();
            List<AlunoDTO> lista = business.Listar();

            cboAluno.ValueMember = nameof(AlunoDTO.ra);
            cboAluno.DisplayMember = nameof(AlunoDTO.nome);
            cboAluno.DataSource = lista;
            //CarregarRA();
        }

        //void CarregarRA()
        //{
        //    AlunoDTO dto = cboAluno.SelectedItem as AlunoDTO;
        //    lblra.Text = dto.ra;
            
           
            
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            AlunoDTO aluno = cboAluno.SelectedItem as AlunoDTO;

         
                BarraDTO dto = new BarraDTO();
                dto.image = ImagePlugin.ConverterParaString(imgbarra.Image);
                dto.IdAluno = aluno.ID;

                BarraBusiness business = new BarraBusiness();
                business.Salvar(dto);
          
                MessageBox.Show("Esse aluno já possui um Código", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            

       
        }

        private void UserControl2_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog local = new SaveFileDialog();
            local.Filter = "Imagens (*.png)|*.png";


            DialogResult resultado = local.ShowDialog();
            if (resultado == DialogResult.OK)
            {
               AlunoDTO dto = cboAluno.SelectedItem as AlunoDTO;
               string mensagem = dto.ra;
                BarCode barcode = new BarCode();
                Image image = barcode.SalvarCodigoBarra(mensagem, local.FileName);
                imgbarra.Image = image;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cboAluno_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CarregarRA();
        }

        private void imgbarra_Click(object sender, EventArgs e)
        {

        }
    }
}
