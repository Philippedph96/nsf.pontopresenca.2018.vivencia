﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.Aluno;
using AlunoRA.Telas.Alterar;

namespace AlunoRA.Telas.Consultar
{
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            AlunoDTO dto = new AlunoDTO();
            //dto.nome = txtaluno.Text;
            AlunoBusiness business = new AlunoBusiness();
            //List<AlunoDTO> lista = business.Consultar(dto);

            string nome = txtaluno.Text;
            List<AlunoDTO> listar = business.Consultar2(nome);
      

            dgvAluno.AutoGenerateColumns = false;
            dgvAluno.DataSource = listar;
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //AlteraAluno frm = new AlteraAluno();

            //if (e.ColumnIndex == 10)
            //{
            //    AlunoDTO dto = dgvAluno.CurrentRow.DataBoundItem as AlunoDTO;
            //    frm.Loadscreen(dto);
            //    Incial form = new Incial();
            //    form.OpenScreen(frm);
            //}
            if(e.ColumnIndex == 10)
            {
                AlunoDTO dto = dgvAluno.Rows[e.RowIndex].DataBoundItem as AlunoDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if(resp == DialogResult.Yes)
                {
                    AlunoBusiness business = new AlunoBusiness();
                    business.Remover(dto);
                  MessageBox.Show("Registro Removido", "Question", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
        }
    }
}
