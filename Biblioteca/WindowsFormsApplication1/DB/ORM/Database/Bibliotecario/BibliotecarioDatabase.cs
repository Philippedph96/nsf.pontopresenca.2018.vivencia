﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Bibliotecario
{
    public class BibliotecarioDatabase
    {
        public int Save(tb_bibliotecario dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_bibliotecario.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_bibliotecario dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_bibliotecario bibliotecario = context.tb_bibliotecario.First(x => x.id_bibliotecario == id);
            context.tb_bibliotecario.Remove(bibliotecario);
            context.SaveChanges();
        }

        public List<tb_bibliotecario> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_bibliotecario.ToList();
        }

        public List<tb_bibliotecario> Filter(string bibliotecario)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_bibliotecario.Where(x => x.nm_bibliotecario.Contains(bibliotecario)).ToList();
        }
    }
}
