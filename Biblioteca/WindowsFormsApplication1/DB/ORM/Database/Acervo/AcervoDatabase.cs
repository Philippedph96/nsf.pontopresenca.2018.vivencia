﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Acervo
{
    public class AcervoDatabase
    {
        public int Save (tb_acervo dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_acervo.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_acervo dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_acervo acervo = context.tb_acervo.First(x => x.id_acervo == id);
            context.tb_acervo.Remove(acervo);
            context.SaveChanges();
        }

        public List<tb_acervo> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_acervo.ToList();
        }

        public List<tb_acervo> Filter(int quantidade)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_acervo.Where(x => x.qt_quantidade == quantidade).ToList();
        }
    }
}
