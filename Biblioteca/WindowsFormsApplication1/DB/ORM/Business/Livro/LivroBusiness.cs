﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.ORM.Database;

namespace WindowsFormsApplication1.DB.ORM.Business
{
    public class LivroBusiness
    {
        public int Save(tb_livro dto)
        {
            LivroDatabase db = new LivroDatabase();
            return db.Save(dto);
        }

        public void Update(tb_livro dto)
        {
            LivroDatabase db = new LivroDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            LivroDatabase db = new LivroDatabase();
            db.Remove(id);
        }

        public List<tb_livro> List()
        {
            LivroDatabase db = new LivroDatabase();
            List<tb_livro> list = db.List();

            return list;
        }

        public List<tb_livro> Filter(string name)
        {
            LivroDatabase db = new LivroDatabase();
            List<tb_livro> list = db.Filter(name);

            return list;
        }
    }
}

