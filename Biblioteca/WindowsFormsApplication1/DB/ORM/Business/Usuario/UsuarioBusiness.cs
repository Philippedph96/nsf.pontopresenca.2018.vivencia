﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.ORM.Database.Usuario;

namespace WindowsFormsApplication1.DB.ORM.Business
{
    public class UsuarioBusiness
    {
        public int Save(tb_usuario dto)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Save(dto);
        }

        public void Update(tb_usuario dto)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            db.Remove(id);
        }

        public List<tb_usuario> List()
        {
            UsuarioDatabase db = new UsuarioDatabase();
            List<tb_usuario> user = db.List();

            return user;
        }

        public List<tb_usuario> Filter(string name)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            List<tb_usuario> user = db.Filter(name);

            return user;
        }
    }
}
