﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Acervo
{
    public class AcervoBusiness
    {
        public int Save (tb_acervo dto)
        {
            AcervoDatabase db = new AcervoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_acervo dto)
        {
            AcervoDatabase db = new AcervoDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            AcervoDatabase db = new AcervoDatabase();
            db.Remove(id);
        }

        public List<tb_acervo> List()
        {
            AcervoDatabase db = new AcervoDatabase();
            List<tb_acervo> list = db.List();

            return list;
        }

        public List<tb_acervo> Filter(int quantidade)
        {
            AcervoDatabase db = new AcervoDatabase();
            List<tb_acervo> list = db.Filter(quantidade);

            return list;
        }
    }
}
