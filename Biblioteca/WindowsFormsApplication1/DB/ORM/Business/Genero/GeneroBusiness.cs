﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Bibliotecario
{
    public class GeneroBusiness
    {
        public int Save(tb_genero dto)
        {
            GeneroDatabase db = new GeneroDatabase();
            return db.Save(dto);
        }

        public void Update(tb_genero dto)
        {
            GeneroDatabase db = new GeneroDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            GeneroDatabase db = new GeneroDatabase();
            db.Remove(id);
        }

        public List<tb_genero> List()
        {
            GeneroDatabase db = new GeneroDatabase();
            List<tb_genero> list = db.List();

            return list;
        }

        public List<tb_genero> Filter(string genero)
        {
            GeneroDatabase db = new GeneroDatabase();
            List<tb_genero> filter = db.Filter(genero);

            return filter;
        }
    }
}
