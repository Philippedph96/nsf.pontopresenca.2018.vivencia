﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Business;

namespace WindowsFormsApplication1.Novas_Telas.Cadastrar
{
    public partial class CadastroUsuário : UserControl
    {
        public CadastroUsuário()
        {
            InitializeComponent();
        }

        private tb_usuario CarregarControles(tb_usuario dto)
        {
            dto.nm_nome = txtNome.Text;
            dto.ds_ano = Convert.ToInt32(txtAno.Text);
            dto.dt_nascimento = Convert.ToDateTime(txtNascimento.Text);
            dto.ds_email = txtEmail.Text;
            dto.ds_celular = txtCelular.Text;
            dto.ds_telefone = txtTelefone.Text;
            dto.ds_ra = txtRA.Text;
            dto.ds_numero = txtNumero.Text;
            dto.ds_cep = txtCEP.Text;
            dto.ds_complemento = txtComplemento.Text;

            return dto;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            tb_usuario dto = new tb_usuario();
            CarregarControles(dto);

            UsuarioBusiness db = new UsuarioBusiness();
            db.Save(dto);
            MessageBox.Show("Novo usuário cadastrado com sucesso!",
                            "Biblioteca Frei",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
    }
}
