﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Business;
using WindowsFormsApplication1.DB.ORM.Database.Autor;
using WindowsFormsApplication1.DB.ORM.Database.Categoria;
using WindowsFormsApplication1.DB.ORM.Database.Sessao;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;

namespace WindowsFormsApplication1.Novas_Telas.Cadastrar
{
    public partial class CadastroLivro : UserControl
    {
        public CadastroLivro()
        {
            InitializeComponent();
            CarregarCombos();
        }
        private void CarregarCombos ()
        {
            //Hadouken... 1º Combo
            AutorBusiness autor = new AutorBusiness();
            List<tb_autor> list = autor.List();
            
            cboAutor.ValueMember = nameof(tb_autor.id_autor);
            cboAutor.DisplayMember = nameof(tb_autor.nm_autor);
            cboAutor.DataSource = list;

            //Shoryuken... 2º Combo
            CategoriaBusiness categoria = new CategoriaBusiness();
            List<tb_categoria> category = categoria.List();

            cboCategoria.ValueMember = nameof(tb_categoria.id_categoria);
            cboCategoria.DisplayMember = nameof(tb_categoria.nm_categoria);
            cboCategoria.DataSource = category;

            //Relâmpago de Plasma... 3º Combo
            SessaoBusiness sessao = new SessaoBusiness();
            List<tb_sessao> session = sessao.List();

            cboSessao.ValueMember = nameof(tb_sessao.id_sessao);
            cboSessao.DisplayMember = nameof(tb_sessao.nm_sessao);
            cboSessao.DataSource = session;

            //Me dê sua força Pegasus, Méteoro de Pegasus... 4º Como
            GeneroBusiness genero = new GeneroBusiness();
            List<tb_genero> genre = genero.List();

            cboGenero.ValueMember = nameof(tb_genero.id_genero);
            cboGenero.DisplayMember = nameof(tb_genero.nm_genero);
            cboGenero.DataSource = genre;

            //Fatality!!!
        }

        private tb_livro CarregarControles(tb_livro dto)
        {
            //Chaves estrangeiras
            tb_autor author = cboAutor.SelectedItem as tb_autor;
            tb_categoria category = cboAutor.SelectedItem as tb_categoria;
            tb_sessao session = cboAutor.SelectedItem as tb_sessao;
            tb_genero genre = cboAutor.SelectedItem as tb_genero;

            //Atribuição de valores as chaves estrangeiras
            dto.id_autor = author.id_autor;
            dto.id_categoria = category.id_categoria;
            dto.id_sessao = session.id_sessao;
            dto.id_gerenro = genre.id_genero;
            
            //Campos primários
            dto.nm_livro = txtNome.Text;
            dto.nm_editora = txtEditora.Text;
            dto.ds_merc = txtMerc.Text;
            dto.ds_espera = txtEspera.Text;
            dto.ds_ano = Convert.ToDateTime(txtAno.Text);
            dto.vl_paginas = Convert.ToInt32(txtPaginas.Text);
            dto.ds_registro = Convert.ToInt32(txtRegistro.Text);
            dto.ds_sumario = txtSumario.Text;
            dto.nm_substituicao = txtSubstituicao.Text;
            dto.ds_assunto = txtAssunto.Text;

            //Retorna o valor dos dados
            return dto;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            tb_livro livro = new tb_livro();
            CarregarControles(livro);

            LivroBusiness book = new LivroBusiness();
            book.Save(livro);

            MessageBox.Show("Livro Salva com Sucesso!!",
                            "NHA Biblioteca",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

        }
    }
}
